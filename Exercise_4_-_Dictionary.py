# -*- coding: utf-8 -*-
"""
Created on Fri Nov  5 14:28:09 2021

@author: JIruthay
"""

'''1. Write a python program to create a Dictionary.'''
def dic(dic1):
    print(dict(dic1))
dic({})

D = {} 
  
L = [['a', 1], ['b', 2], ['a', 3], ['c', 4]]
  
# Loop to add key-value pair
# to dictionary
for i in range(len(L)):
    # If the key is already 
    # present in dictionary
    # then append the value 
    # to the list of values
    if L[i][0] in D:
        D[L[i][0]].append(L[i][1])
      
    # If the key is not present
    # in the dictionary then add
    # the key-value pair
    else:
        D[L[i][0]]= []
        D[L[i][0]].append(L[i][1])
          
print(D) 

'''2.Write a python program to access the values in the Dictionary.'''
def accs(dic1):
    print(dict(dic1))
accs({"name":"keerthana","age":27})

'''3.Write a python program to update the above created dictionary.'''

def uptd(dic1):
    dic1.update({"country":"india"})
    print((dic1))
uptd({"name":"keerthana","age":27})

'''4.Write a python program to delete the above created dictionary.'''

def dele(dict1):
    dict2 =(dict1.clear())
    dict3 = dict(dict1)
    print(dict3)
dele({"name":"keerthana","age":27,"country":"india"})

'''5. Write a python code to copy the entire dictionary into a new dictionary.'''
def cpy(dict1)
    print("original dict1:",dict1)
    a = dict1.copy()
    print("new dict:",a)
cpy({"name":"keerthana","age":27,"country":"india"})

'''6. Write a python code to delete keys from the dictionary.'''

def keydel(dict1):
    a=dict1.pop("name")
    print(a)
keydel({"name":"keerthana","age":27,"country":"india"})


test_dict = {"Arushi" : 22, "Anuradha" : 21, "Mani" : 21, "Haritha" : 21}
print ("The dictionary before performing remove is : " + str(test_dict))
del test_dict['Mani']
print ("The dictionary after remove is : " + str(test_dict))



''' 7. Write a python code to sort the elements in the dictionary'''
dic={2:90, 1: 100, 8: 3, 5: 67, 3: 5}
dic2={}
for i in sorted(dic):
   dic2[i]=dic[i]
print(dic2)
